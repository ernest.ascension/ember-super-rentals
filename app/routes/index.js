import Route from '@ember/routing/route';

import { inject as service } from '@ember/service';

export default class IndexRoute extends Route {
  // access data with ember service
  @service store;

  async model() {
    return this.store.findAll('rental');
  }
}