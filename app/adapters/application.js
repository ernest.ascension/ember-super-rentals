// Needed for JSON API impelentation
import JSONAPIAdapter from '@ember-data/adapter/json-api';

export default class ApplicationAdapter extends JSONAPIAdapter {
  // Path specification
  namespace = 'api';

  // Format URL using super built-in buildURL method and pass in route params (may include id as route)
  buildURL(...args) {
    return `${super.buildURL(...args)}.json`;
  }
}