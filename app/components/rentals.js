import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export default class RentalsComponent extends Component {
  // Uses tracked to detect changes and re-render
  @tracked query = '';
}