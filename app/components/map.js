import Component from '@glimmer/component';
// Import ENV for API reference (Check config/environment.js)
import ENV from 'super-rentals/config/environment';

// MAPBOX API definition
const MAPBOX_API = 'https://api.mapbox.com/styles/v1/mapbox/streets-v11/static';

export default class MapComponent extends Component {
  // Formats and returns API request
  get src() {
    // Takes all props from component
    let { lng, lat, width, height, zoom } = this.args;

    let coordinates = `${lng},${lat},${zoom}`;
    let dimensions  = `${width}x${height}`;
    // Gets token by executing get token() method
    let accessToken = `access_token=${this.token}`;

    // Return according to MAPBOX API documentation
    return `${MAPBOX_API}/${coordinates}/${dimensions}@2x?${accessToken}`;
  }

  // Returns Token obtained from env
  get token() {
    return encodeURIComponent(ENV.MAPBOX_ACCESS_TOKEN);
  }
}